#!/usr/bin/env python3.7
""" A set of classes to scan for available wireless networks. 
    This file is not intended to run as __main__.

    iwlist():
        Is the main class of this module. It inherits all the methods and 
        attributes from the CellParser class to parse the Linux command 
        iwlist <interface> scanning."""
from sys import stdout
from re import compile as reCompile, findall as reFindall
from psutil import net_if_addrs, net_if_stats, subprocess as subP

class CellParser():
    """ This class's sole function is to aid iwlist in parsing the cell data."""

    def __init__(self):
        """ We need to set up some class attributes. """
        ## apRgx is the main regex. Gets almost everything. It's greedy but 
        ## imperfect. Just what we need, compile it.
        self._apRgx = reCompile(r'\s*(.*)[:](.*)')
        ## essidRgx gets the ESSID string from the cell. Compile it'ski.
        self._essidRgx = reCompile(r'\s*ESSID:"(.*)"')
        ## qualRgx gets the Quality and Signal level from the cell. Compile it.
        self._qualRgx = reCompile(r'\s*(Quality=.*)')
        ## bitRgx gets the Bit Rates string from the cell. Compile them.
        self._bitRgxs = (reCompile(r'\s*Bit Rates:(.*)\n\s*(.*)\n\s*[A-z]'),
                         reCompile(r'\s*Bit Rates:(.*)'))
        ## We are going go need to clean up a couple keys. We will be adding
        ## them to a different key. This ieKey will hold that key name.
        self._ieKey = ''


    def _parse_address(self, _dict, _key, _val):
        """ This method parses the address information from the cell data. """
        if ' - ' in _key:
            ## We found the Address key:value. It's a mess. Clean that up man.
            nVal = ':'.join([_key, _val]).split('Address: ')[1]
            nKey = _key.split(' - ')[1].split(': ')[0]
            ## Add newly cleaned up key:value to the master
            _dict.insert_before(_key, nKey, nVal)
            ## remove broken Address key:value
            del _dict[_key]


    def _parse_essid(self, _dict, _key, _cell):
        """ This method parses the ESSID string cleaning the extra ' " '. """
        if _key[:5] == 'ESSID':
            essid = reFindall(self._essidRgx, _cell)
            if len(_key) == 5:
                ## the ESSID's have extra quotes that need to be removed.
                _dict[_key] = essid[0]
            else:
                ## We have found the rare instance where the user chose a ESSID
                ## that has a ":" in it, like a smiley face :) or whatnot.
                _dict['ESSID'] = essid[0]
                ## remove the unexpected key
                del _dict[_key]


    def _parse_bit_rates(self, _dict, _cell):
        """ This method attempts to parse the bit rates. """
        ## Bit of regex magic to get the Bit Rates from a split line.
        bitRates = reFindall(self._bitRgxs[0], _cell)
        for bitSet in bitRates:
            for bitRate in bitSet:
                if isinstance(_dict['Bit Rates'], str):
                    _dict['Bit Rates'] = tuple(x.strip() for x in bitRate.split(';'))
                else:
                    _dict['Bit Rates'] += tuple(x.strip() for x in bitRate.split(';'))

        ## A bit more magic to get the rest of there are any.
        bitRates = reFindall(self._bitRgxs[1], _cell)
        if len(bitRates) >= 2:
            for bitRate in bitRates[1:]:
                _dict['Bit Rates'] = (_dict['Bit Rates'],
                                      tuple(x.strip() for x in bitRate.split(';')),)


    def _parse_extra(self, _dict, _key, _val):
        """ This method parses the two Extra keys, each of which must be split
        with different delimiter and one key, val created and added to the
        preexisting key. """
        if _key == 'Extra':
            if not isinstance(_val, dict):
                newVal = _val.split('=')
                _dict[_key] = {newVal[0]: newVal[1]}
            else:
                _dict[_key].update({newVal[0]: newVal[1]})

        if 'Extra:' in _key:
            newKey = _key.split(':')[1].strip()
            if isinstance(_dict['Extra'], dict):
                _dict['Extra'].update({newKey: _val.strip()})
            else:
                _dict['Extra'] = {newKey, _val.strip()}

            del _dict[_key]


    def _parse_ie(self, _dict, _key, _val):
        """ This method parses the known IE information from the cell data. """
        lookFor = ['Group Cipher', 'Pairwise Ciphers', 'Authentication Suites']
        if _key == 'IE' or 1 in [1 for x in lookFor if x in _key]:
            ## We have found a bunch of things that need to be joined 
            ## together.
            if _key == 'IE':
                ## The ieKey will be our new key for the next couple of loops.
                self._ieKey = _val

                if isinstance(_dict[_key], dict):
                    ## We have setup already. Just update.
                    _dict[_key].update({self._ieKey: dict()})
                else:
                    ## create new dict for our new values
                    _dict[_key] = {self._ieKey: dict()}
            else:
                ## We found some key:values that should be values of 
                ## IE[self._ieKey] not keys of their own. Add them to master.
                _dict['IE'][self._ieKey].update([(_key, _val)])
                ## Remove the old key
                if _key in _dict.keys():
                    del _dict[_key]


    def _parse_unknown_ie(self, _dict, _key, _val):
        """ This method parses the Unknown IE information form the cell data."""
        if _key == 'IE: Unknown':
            ## Here wee will grab all the IE: Unknown knowns.
            if not 'IE Unknown' in _dict.keys():
                _dict['IE Unknown'] = (_val.strip(),)
            else:
                _dict['IE Unknown'] += (_val.strip(),)


class iwlist(CellParser):
    """ This class will scan for wireless access points on linux systems using
        iwlist. iwlist class expects a keyword argument aka kwarg of
        interface=[access point]. like wlp3s0, wlan0, etc.. If no interface
        kwarg class will run through a basic CLI setup asking what Access Point
        to use. To bypass all setup set: interface=None ."""

    def __init__(self, **krgs):
        """ Build self. This method expects a keyword argument of interface as
        it's key. The value must be a valid network interface name. wlanX or
        wlp3sX, etc... See self.cards() method for available interface names.

        Example:
            self.interface = 'wlan0'"""

        ## Set up the cell parser attributes
        CellParser.__init__(self)

        ## Set the default wireless interface to None.
        self.interface = None

        ## Set the default list output.
        self.list = None

        if 'interface' not in krgs.keys():
            ## User has not passed network interface to use. Ask questions
            self.build()
            ## Scan for interface's and build self.list
            self.scan()
        else:
            ## Set the object's interface variable.
            if krgs['interface'] is not None and krgs['interface'] in self.cards():
                self.interface = krgs['interface']
                ## Scan for access points and build self.list
                self.scan()
            else:
                if krgs['interface'] is not None:
                    raise InterfaceError(krgs['interface'])


    def cards(self, names=True):
        """ Returns all the available wifi cards as a list. 
        Example:
            self.cards() ## will return something like,
            ['lo', 'enp4s0', 'wlp3s0']
        
          Alternatively to gain more info you can pass names=False. 

            self.cards(names=False)

              Above will return each interface's snicaddr() information. Family,
              address, netmask, broadcast and ptp."""

        if names:
            return [x for x in net_if_addrs()]

        ## Dont waste it. show them everything.
        return net_if_addrs()


    def is_up(self, card):
        """ Checks if wireless card is available and up. 
        Returns Boolean.
            Example:
                True if self.is_up('wlp3s0') else False

                  *InterfaceError raised if card is not in self.card()."""

        ## If card is in available interface list
        if card in self.cards():
            ## return Boolean if card is up or not
            return net_if_stats()[card].isup

        raise InterfaceError(card)


    def service(self, card, direction):
        """ This method controls the wifi cards up or down state. 
        Returns boolean.
            Example:
                True if self.service('wlp3s0', 'up') else False

          *InterfaceError raised if card is not in self.card().

          *InterfaceDirectionError raised if direction is not 'up' or 'down'.

        ** Requires root privileges."""

        if not card in self.cards():
            raise InterfaceError(card)

        if str(direction).lower() in ['up', 'down']:
            ## Set the command to bring the network interface up.
            cmd = ['sudo', 'ifconfig', card, direction]
            ## quantum truthnessity, proc_read might need a rethink?
            return True if proc_read(cmd)[1] == True else False
        
        raise InterfaceDirectionError(direction)


    def scan(self):
        """ self.scan() will scan for available network access points and
        create self.list MutableDict object. 

          *InterfaceError raised if self.interface is not in self.card().

        ** Requires root privileges."""

        ## Make sure the wireless card selected is present and up.
        if not self.is_up(self.interface):
            raise InterfaceError(self.interface)

        ## set default value for self.list.
        self.list = None
        ## Try to scan for networks and create self.list dictionary
        try:
            ## Scan for access points using iwlist [interface] scanning and 
            ## split output by: Cell.
            p1 = subP.Popen(['sudo', 'iwlist', self.interface, 'scanning'], 
                            stdout=subP.PIPE).communicate()[0].decode().split('Cell')
            ## set up the self.list 
            self.list = {i: self._strip_cell(x) for i, x in enumerate(p1)}
        except Exception as e:
            ## Still waiting for one to happen! Thats the reason for the 
            ## general catchall. Was thinking PermissionError or ValueError?
            raise e
        else:
            ## Update self.list object
            ## 'Scan completed' is the first line in returned from Linux's
            ## iwlist [interface] scanning
            del self.list[0] ## Always a None. First key is rubbish anyway.


    def sift(self, count, search, single=False):
        """self.sift is a quick sorter and limiter. Returns a list of listed 
        dicts or None.

        Args:
            count:
                Will limit the amount of returned nodes. If count is 0 will
                return all cells. If negative count will be self.list - count .
            search:
                Is a list of keys to search through for.
            single:
                If *single is anything other than False'y: the *count arg will 
                become a key's value of the self.list, thus searching only that
                specific key.

        Example 1 - [single=False]:
            self.sift(2, ['Address', 'ESSID']) ## will return something like
                [{'Address': '72:6A:G5:BC:96:AD',
                  'ESSID': ''},
                 {'Address': 'GR:8C:MY:ED:1T:58',
                  'ESSID': 'FBI_van_#-5'}]

        Example 2 - [single=True]:
            self.sift(6, ['ESSID', 'Address'], True) ## will return the likes of
                {'Address': 'AG:U4:A6:0R:GE:E7',
                 'ESSID': 'Brian, Clean your room'}"""

        count, search = _validate_sift_args(count, search)

        if self.list is not None:
            if count > len(self.list):
                raise IWlistIndexError(count)

            lmted = []
            if single:
                if count <= 0:
                    raise IWlistIndexError(count)

                lmted.append({y: z for y, z in self.list[count].items() if _str_in_list(y, search)})
                return lmted[0] if lmted[0] != {} else None
            else:
                count = len(self.list) if count == 0 else count
                for x in list(self.list.values())[:count]:
                    lmted.append({y: z for y, z in x.items() if _str_in_list(y, search)})
                return lmted if lmted[0] != {} else None
        else:
            return None


    def list_of(self, search=None):
        """ Returns a tuple from a limited search of self.list 

            Example 1:
                ## return a tuple of all possible search keys.
                self.list_of() 

            Example 2:
                ## return a tuple of values based on search.
                self.list_of('ESSID') ## will return something like,
                ('MyESSID', 'TheGuyDownTheRoad', 'YourMomsHouse')"""

        if self.list is None:
            return None
        if search is not None:
            try:
                search = search.upper() if search.lower() in ['essid', 'ie'] else search.title()
                search = 'IE Unknown' if 'unknown' in search.lower() else search
                return tuple(y[search] for y in self.sift(0, [search]) if search in y.keys())
            except TypeError as e:
                raise IWlistKeyError(search, self.list_of())
        else:
            return tuple(x for x in self.list[1].keys())


    def build(self, quiet=False):
        """ self.build will help user set their wireless card. This whole
        method can be removed if your program doesn't require any user 
        interaction. Or better you can override it with super() in your GUI."""

        ## get available network connection interfaces.
        cons = self.cards()

        ## Create a string from the the key value pair. Will be used in
        ## questions below. key[Number]: value[String:ESSID]

        questionStr = ''
        for i, val in enumerate(cons, 1):
            questionStr = f'{questionStr}\n  {i}: {val}'

        while True:
            ## Ask about the interface?
            try:
                interface = input(f"""{questionStr}\n
 Which interface do you want to use?:\n> """)
            except (KeyboardInterrupt, SystemExit) as e:
                raise e
            else:
                if interface.isdigit():
                    ## If is number get the corresponding network interface.
                    interface = cons[int(interface)-1]

                if not self.is_up(interface):
                    ## Network interface is not up. Bring the service up
                    self.service(interface, 'up')
                    ## Double check the status of the wireless interface
                    if not self.is_up(interface):
                        raise InterfaceError(interface)
                    else:
                        ## Set the object's interface variable and exit loop.
                        self.interface = interface
                        break
                else:
                    ## Network interface is up so set the object's interface
                    ## variable and exit loop.
                    self.interface = interface
                    break

        if not quiet:
            print(f'\n  Using interface: {self.interface}')


    def _strip_cell(self, cell):
        """ StripCell is a Method that parses a cell from Linux:
            iwlist <interface> scanning

        Method. Returns a MutableDict object or None if not parsable. """

        ## Use a little regex to find almost all our key value pairs. Will
        ## require a little clean up.
        cellSplit = reFindall(self._apRgx, cell)
        
        ## Create the muDict MutableDict. This is the template for the 
        ## return dict.
        muDict = MutableDict({x.strip(): y for x, y in cellSplit})

        if not 1 in [1 for x in muDict.keys() if 'Scan completed' in x]:
            ## Loop through the regex.reFindall aka cellSplit
            for key, val, in cellSplit:

                self._parse_address(muDict, key.strip(), val.strip())

                self._parse_essid(muDict, key.strip(), cell)

                self._parse_unknown_ie(muDict, key.strip(), val.strip())

                self._parse_ie(muDict, key.strip(), val.strip())

                self._parse_extra(muDict, key.strip(), val.strip())

            self._parse_bit_rates(muDict, cell)

            ## Do a little regex magic to get the Quality and Signal Levels.
            qualChk = reFindall(self._qualRgx, cell)[0].split('Signal ')
            ## create Quality key:var from qualChk's first value
            qual = qualChk[0].split('=')
            ## Add Quality key:val pair to muDict after Frequency key
            muDict.insert_after('Frequency',
                                qual[0].strip(),
                                qual[1].strip())
            ## Add the Signal Level key:value to the muDict after Quality
            muDict.insert_after('Quality',
                                'Signal Level',
                                qualChk[1].split('=')[1].strip())
            ## Make the 'Encryption key' key title case for key consistency
            muDict.insert_after('Encryption key',
                                'Encryption Key',
                                muDict['Encryption key'])
            ##remove old 'Encryption key' from dictionary.
            del muDict['Encryption key']
            ## Remove the IE: Unknown key. it's already in "IE Unknown"
            del muDict['IE: Unknown']
            ## were done. Get rid of it.
            return muDict
        
        return None


class MutableDict(dict):
    """ Class that extends python3.7 dictionary to include insert_before and
    insert_after methods. """

    def insert_before(self, key, newKey, val):
        """ Insert newKey:value into dict before key """
        try:
            _keys = list(self.keys())
            _vals = list(self.values())

            insertAt = _keys.index(key)

            _keys.insert(insertAt, newKey)
            _vals.insert(insertAt, val)

            self.clear()
            self.update({x: _vals[i] for i, x in enumerate(_keys)})

        except ValueError as e:
            raise e


    def insert_after(self, key, newKey, val):
        """ Insert newKey:value into dict after key """
        try:
            _keys = list(self.keys())
            _vals = list(self.values())

            insertAt = _keys.index(key) + 1

            if _keys[-1] != key:
                _keys.insert(insertAt, newKey)
                _vals.insert(insertAt, val)
                self.clear()
                self.update({x: _vals[i] for i, x in enumerate(_keys)})
            else:
                self.update({newKey: val})

        except ValueError as e:
            raise e


class InterfaceError(Exception):
    """ This class handles what to do when the interface is not set and the 
    self.scan() is run. """

    def __init__(self, txt):
        """ Raise our custom error. """
        self.text = f'Wireless Interface Error -> "{txt}" is not a valid ' 
        self.text += 'wireless interface.\n    Possible interface names:'
        self.text += f" {', '.join([x for x in net_if_addrs()])}. Not all"
        self.text += " support scanning."

        super().__init__(self)

    def __str__(self):
        return self.text


class InterfaceDirectionError(Exception):
    """ This class handles what to do when the interface direction is not set 
    "up" or "down"."""

    def __init__(self, txt):
        """ Raise our custom error. """
        self.text = f'Wireless Interface Direction Error -> "{txt}" is not '
        self.text += "up or down."

        super().__init__(self)

    def __str__(self):
        return self.text


class IWlistKeyError(Exception):
    """ This class handles what to say when there is a key error usually a
    NoneType error. """

    def __init__(self, txt, keys):
        """ Raise our custom error. """
        self.text = f'TypeError: -> "{txt}" is not a valid iwlist object key.\n'
        self.text += f"  Keys: {', '.join(keys)}."

        super().__init__(self)

    def __str__(self):
        return self.text


class IWlistIndexError(Exception):
    """ This class handles what to say when there is a key error in the 
    iwlist.sift() methods index. """

    def __init__(self, txt):
        """ Raise our custom error. """
        if not isinstance(txt, int):
            self.text = f'TypeError: -> "{txt}" is not a valid iwlist object '
            self.text += f'index.\n    Expected integer, "{txt}" is '
            self.text += f'{type(txt)}.'
        else:
            self.text = f'Index: {txt} <- does not exist.'

        super().__init__(self)

    def __str__(self):
        return self.text


def proc_read(cmd):
    """ Read progress in real time to stdout and log output for return.
    It's all that and then some, small dark and handsome. Bust a stdout
    in you're eye to show you what I stdin from."""

    process = subP.Popen(cmd, bufsize=1, universal_newlines=True,
                         stdout=subP.PIPE, stderr=subP.STDOUT)
    outPut = ''
    for line in iter(process.stdout.readline, ''):
        if line != '':
            outPut = f'{outPut}\n{line}'
            print(line.strip())
        stdout.flush()
    process.wait()
    errcode = process.returncode
    return (outPut, True) if errcode == 0 else (outPut, errcode)


def _validate_sift_args(_count, _list):
    """ This function will test the arguments passed to sift for errors and 
    raise them if found. """

    if not isinstance(_count, int):
        raise IWlistIndexError(_count)

    if not isinstance(_list, list):
        _list = [str(_list)]

    return _count, _list


def _str_in_list(_str, _list):
    """ This will help parse users list and string. Attempting to aid the user
    in matching case sensitive keys; this function first tries the case 
    sensitive search and then normalizes both string and list strings to 
    lowercase. Returns True if string is in list."""

    if _str in _list:
        return True
    elif _str.lower() in [x.lower() for x in _list]:
        return True
    else:
        return False
