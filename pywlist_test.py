#!/usr/bin/env python3.7
""" This file is to setup to test the accuracy of iwlist.list dictionary 
    object values and their continuity with the *nix command 
    iwlist <interface> scanning's output by saving a json object and a text
    file for human comparison. 

    It saves the files in their own personal directories inside the "test" 
    directory; labeled using random datetime string naming convention which
    the files will share. 

        Usage:

            python3.7 pywlist.py"""

from os import mkdir as oMkdir
from os.path import (dirname as oDirname, abspath as oAbspath,
                     exists as oExists, join as oJoin)
import subprocess as subP
from datetime import datetime as dTime
from json import dumps as jDumps
from pywlist import iwlist

class iwtest(iwlist):
    """ This class is setup to test the accuracy of iwlist.list dictionary 
    object values and their continuity with the *nix command 
    iwlist <interface> scanning's output. 

    As there is no good way besides human comparison I haver added two methods
    to extend iwlist().scan() class which is also rewritten to include them.
    They both save a copy of the data. One in json and the other in text. """

    def __init__(self, **karg):
        """
        Usage:

            ## import the test
            from pywlist_test import iwtest

            ## Setup and run test
            iw = iwtest()

            ## Test is executed on scan so to rerun test one must scan
            iw.scan()"""

        self.base_dir = oAbspath(oDirname(__file__))
        ## setup our test directory. It will hold our individual tests.
        self.testDirs = oJoin(self.base_dir, 'tests')
        ## if the test directory does not exist
        if not oExists(self.testDirs):
            ## make the directory
            oMkdir(oJoin(self.testDirs))

        ## Im going to use datetime to generate unique filenames but will set
        ## in self.now() method when we create it's directory. None will do to
        ## start with.
        self.filename = None

        ## Run the parent iwlist class's __init__ to setup and run our initial
        ## scan and write our first test files. 
        iwlist.__init__(self, **karg)


    def now(self):
        """Will return a unique filename directory and update self.filename ."""

        ## Im going to use datetime to generate unique filenames 
        _now = dTime.now()
        _format = '%d-%m-%Y_%H:%M:%S'
        self.filename = f'{_now.strftime(_format)}'

        ## Since we are going to be making two files to compare to each other
        ## they should be held in their own respective directories. Here we 
        ## will make them if they do not exist.
        if not oExists(oJoin(self.testDirs, self.filename)):
            oMkdir(oJoin(self.testDirs, self.filename))


    def save_json(self, _dict):
        """ This method will write a .json file from the provided dictionary
        object """
        with open(oJoin(self.testDirs, self.filename,
                        f'{self.filename}.json'), 'w') as wFile:
            wFile.write(jDumps(_dict, indent=4))


    def save_text(self, txt):
        """ This method will write a the outupt from the *nix command 
        iwlist <interface> scanning to a text file """
        with open(oJoin(self.testDirs, self.filename,
                        f'{self.filename}.txt'), 'w') as wFile:
            wFile.write(txt)


    def scan(self):
        """ self.scan() will scan for available network access points and
        create self.list MutableDict object. 
        NoneType error if self.interface is None or invalid interface name.

        ** Requires root privileges."""

        self.list = None

        ## run a catch to make sure the wireless card selected is present and
        ## up.
        try:
            ## Scan for access points using iwlist [interface] scanning and 
            ## split output by: Cell. This time we just want the text.
            p1 = subP.Popen(['sudo', 'iwlist', self.interface, 'scanning'], 
                            stdout=subP.PIPE).communicate()[0].decode()

            ## setup directory and self.filename based on datetime.now()
            self.now()

            ## here we will use our new save method we created for testing 
            self.save_text(p1)

            ## Now that we have saves our text file we can split our cells.
            p1 = p1.split('Cell')

            ## set up the self.list 
            self.list = {i: self._strip_cell(x) for i, x in enumerate(p1)}

            ## now that we have our self.list object we need to save it
            ## to with our json saver method for comparison later.
            self.save_json(self.list)

        except Exception as e:
            ## Still waiting for one to happen! Thats the reason for the 
            ## general catchall. 
            raise e
        else:
            ## Update self.list object
            ## 'Scan completed' is the first line in returned from *nix iwlist
            ## iwlist [interface] scanning
            del self.list[0] ## Always a None. First key is rubbish anyway.


if __name__ == '__main__':

    ## we are running as a file not as import so run the iwtest just like
    ## iwlist to make our comparison files.

    iw = iwtest()
