# pylist

A python 3.7+ class to view access points available to your wireless card in a Linux environment.

iwlist class will provide a very basic environment to choose your Wireless network card for scanning and scan for available network access points.


## Installation

The iwlist class needs the psutil library to work. If iwlist doesn't run try to install psutil with:

1. Make sure you have python3.7 installed. I think deadsnakes has a repo you can add to debian.

2. Create a virtual environment.

3. Install psutil:

```Bash

## Works in venv. Cant install pip for 3.7 easily on 16.04. 

## install python3.7 pip
sudo apt install python3.7-venv

## change to your directory you want to make a virtual python environment.
cd py3-7_working

## Make your venv.
python3.7 -m venv .

## activate your venv
source bin/activate

## install psutil
python3.7 -m pip install psutil

```


# Basic list usage:

You can use like this if you don't require user interaction.

```Python

from pywlist import iwlist

## Create iwlist object. Will prompt for sudo if not root
## Creating the object will do initial access point scan unless you pass None.
## We'll start with a clean object to build manually.
iw = iwlist(interface=None)

## If you don't know what card to use you can list available cards
iw.cards()
```
```Bash
Out:  ['lo', 'enp4s0', 'wlp3s0']
```
```Python

## You can get a whole lot more info than just connection interface names.
iw.cards(names=False)
```
```Bash
Out: 
{ 'lo': [snicaddr(family=<AddressFamily.AF_INET: 2>, address='127.0.0.1', netmask='255.0.0.0', broadcast=None, ptp=None),
 snicaddr(family=<AddressFamily.AF_INET6: 33>, address='::1', netmask='ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff', broadcast=None, ptp=None),
 snicaddr(family=<AddressFamily.AF_PACKET: 69>, address='00:00:00:00:00:00', netmask=None, broadcast=None, ptp=None)],
 'enp4s0': [snicaddr(family=<AddressFamily.AF_PACKET: 17>, address='2g:0r:!4:f4:y0:u2', netmask=None, broadcast='ff:ff:ff:ff:ff:ff', ptp=None)],
 'wlp3s0': [snicaddr(family=<AddressFamily.AF_PACKET: 17>, address='03:09:f8:b4:61:15', netmask=None, broadcast='ff:ff:ff:ff:ff:ff', ptp=None)] }

```
```Python

## Set the wireless card using the dictionary
iw.interface = 'wlp3s0'
print(iw.interface)
```
```Bash
Out: 'wlp3s0'
```
```Python

## Now we can check if the wireless card is up and if not bring it up.
if iw.is_up(iw.interface):
    pass
else:
    ## the state for the interface is down. Bring it up
    iw.service(iw.interface, 'up')
```
```Bash
Out: True
```
```Python

## Everything is set up and ready. Start scan.
iw.scan()

## Now we can list our access points we just scanned.
print(iw.list)
```
```Bash
Out:
 19: {'Address': '90:EF:68:56:14:63',
  'Channel': '6',
  'Frequency': '2.437 GHz (Channel 6)',
  'Quality': '40/70',
  'Signal level': '-70 dBm',
  'Encryption key': 'on',
  'ESSID': "theCreepyGuyWithTheTelephotoLense",
  'Bit Rates': (('1 Mb/s', '2 Mb/s', '5.5 Mb/s', '11 Mb/s', '6 Mb/s', '9 Mb/s', '12 Mb/s', '18 Mb/s'),
                ('24 Mb/s', '36 Mb/s', '48 Mb/s', '54 Mb/s')),
  'Mode': 'Master',
  'Extra': {'tsf': '0000031e4827480a', 'Last beacon': '632ms ago'},
  'IE': {'IEEE 802.11i/WPA2 Version 1': {'Group Cipher': 'CCMP',
    'Pairwise Ciphers (1)': 'CCMP',
    'Authentication Suites (1)': 'PSK'},
   'WPA Version 1': {'Group Cipher': 'CCMP',
    'Pairwise Ciphers (1)': 'CCMP',
    'Authentication Suites (1)': 'PSK'}},
  'IE Unknown': ('00124B61736869277320636F6E6E656374696F6E',
   '010882848B960C121824',
   '030106',
   '0706555320010B1E',
   '2A0100',
   '32043048606C',
   '460573D000000C',
   '2D1AAD0903FFFF000000000000000000000100000000000000000000',
   '3D1606000000000000000000000000000000000000000000',
   '7F0A04000F02000000400040',
   'BF0C92398933FAFF0000FAFF0020',
   'C005000600FCFF',
   'FF1A230D01081A400000604C891FC1839C010800FAFFFAFF191CC771',
   'FF0724F43F0006FCFF',
   'FF03270500',
   'FF0E260603A42827A428427328627228',
   'DD180050F2020101000003A4000027A4000042435E0062322F00',
   'DD0900037F01010000FF7F',
   'DD168CFDF0040000494C51030209720100000000FEFF0000',
   'DD820050F204104A0001101044000102103B00010310470010876543219ABCDEF01234D0768F1AF0161021000543616C6978
10230007475334323230451024000233321042000C3432323030373133343935331054000800060050F20400011011000C43584E
4B3030383731324238100800020000103C0001011049000600372A000120',
   'DD088CFDF00101020100',
   'DD138CFDF001010201000201010303010100040101')},

 20: {'Address': '72:G2:8A:EC:D9:04',
  'Channel': '1',
  'Frequency': '2.412 GHz (Channel 1)',
  'Quality': '31/70',
  'Signal level': '-79 dBm',
  'Encryption key': 'on',
  'ESSID': '',
  'Bit Rates': ('6 Mb/s', '9 Mb/s', '12 Mb/s', '18 Mb/s', '24 Mb/s', '36 Mb/s', '48 Mb/s', '54 Mb/s'),
  'Mode': 'Master',
  'Extra': {'tsf': '00000228d102209a', 'Last beacon': '940ms ago'},
  'IE': {'IEEE 802.11i/WPA2 Version 1': {'Group Cipher': 'CCMP',
    'Pairwise Ciphers (1)': 'CCMP',
    'Authentication Suites (1)': 'PSK'}},
  'IE Unknown': ('0000',
   '01088C129824B048606C',
   '030101',
   '050400010000',
   '0706555320010B80',
   '2A0104',
   '2D1A2C091FFF00000000000000000096000100000000000000000000',
   '3D1601000000000000000000000000000000000000000000',
   'DD180050F2020101810003A4000027A4000042435D0061112E00',
   'DD3A0050F204104A00011010440001021049000600372A00012010110012526F6B752045787072657373202D204A52501054
00800070050F2040001',
   'DD12506F9A09020200210B030600125932531DBC',
   'DD0D506F9A0A00000601111C440032')}

```

You can rescan by:

```Python

## rescan the network using the same access point.
iw.scan()

## and reprint new scan.
print(iw.list)

```

If you want the user to choose what wireless card they want to use just don't pass the interface kwarg to iwlist in creation.

Example:

```Python

## import iwlist
from pywlist import iwlist

## create with user input.
iw = iwlist()

## The user will be prompted with something like this.

```

```Bash

  1: wlp3s0

  2: emp4s0

Which interface do you want to use?:

```

You can type the number: 1, or type the interface name: enp4s0.

You can also type exit or [Ctl]+c to exit the question. 
If you need to test if user has not entered a wireless card by exiting with exit do:

```Python

if iw.interface is not None:
    ## user has selected a wireless card.

```

You can reselect your wireless card to scan with by:

```Python

## user reselect your wireless card
iw.build()

## manually reselect your wireless card
iw.interface = 'wlp3s0'

## you will have to manually rescan to update list with
## new wireless card scan.
iw.scan()

## Print new scan from new wireless card.
print(iw.list)

``` 

You can get the wireless interface the user selected by:

```Python

## user has selected wireless interface but we need it for our code
## somewhere else.
winterface = iw.interface

print(winterface)
```
```Bash
Out: 'wlp3s0'
```

## Note

Use numbers when accessing the interface nodes. Also count; like on your fingers. 

```Python

## use list iteration; if you want to get the first four items. Like a machine.
list(iw.list[1].items())[:4]
```
```Bash
Out:
[('Address', '8C:59:73:12:AB:E1'),
 ('Channel', '9'),
 ('Frequency', '2.452 GHz (Channel 9)'),
 ('Quality', '52/70')]

```
```Python

## as you see above I used the iw.list[1] << key is number. 
## The numbers start counting at one not zero!

## Pick a specific wireless access point

# here we just grab the first element.
list(iw.list.values())[0]     

## the above is the same as using a direct call to number. Really, a number!
## Count like a people!
iw.list[1] ## is equivalent to list(iw.list.values())[0]

```
```Bash
## shortened sample output of the first cell
Out:
[ {'Address': '72:G2:8A:EC:D9:04',
  'Channel': '1',
  'Frequency': '2.412 GHz (Channel 1)',
  'Quality': '31/70',
  'Signal level': '-79 dBm',
  'Encryption key': 'on',
  'ESSID': '',
  'Bit Rates': ('6 Mb/s', '9 Mb/s', '12 Mb/s', '18 Mb/s'),
  'Mode': 'Master',
  'Extra': {'tsf': '00000228d102209a', 'Last beacon': '940ms ago'},
  'IE': {'IEEE 802.11i/WPA2 Version 1': {'Group Cipher': 'CCMP',
    'Pairwise Ciphers (1)': 'CCMP',
    'Authentication Suites (1)': 'PSK'}},
  'IE Unknown': ('0000')} ]
```

```Python

## you can test with:
True if list(iw.list.values())[0] == iw.list[1] else False ## will be True

## you cant do 
iw.list[5:] # it will fail. iw.list is a dictionary not a list. 
```

# More:

```Python

## Also there is a sift method you can use for a basic Keys sort:
## First arg is the count. How many nodes to show. If 0 will print all
## The second is the list of keys you want to find.
iw.sift(2, ['Quality', 'Address', 'ESSID', 'Channel', 'Frequency', 'Encryption Key'])
```
```Bash
Out:
[ {'Address': '72:6A:G5:BC:96:AD',
  'Channel': '149',
  'Frequency': '5.745 GHz',
  'Quality': '31/70',
  'Encryption Key': 'on',
  'ESSID': ''},
 {'Address': 'GR:8C:MY:ED:1T:58',
  'Channel': '149',
  'Frequency': '5.745 GHz',
  'Quality': '31/70',
  'Encryption Key': 'on',
  'ESSID': 'FBI_van_#-5'} ]
```
```Python

## You can sift search a single key from the list.
## When searching a single key the third arg must not be False.
## The first arg becomes the key you want to sift.
## The second arg is still the list of keys you want to find.
iw.sift(6, ['ESSID', 'Address', 'IE'], True)
```
```Bash
{'Address': 'AG:U4:A6:0R:GE:E7',
 'ESSID': 'Brian, Clean your room',
 'IE': {'IEEE 802.11i/WPA2 Version 1': {'Group Cipher ': ' CCMP',
   'Pairwise Ciphers (1) ': ' CCMP',
   'Authentication Suites (1) ': ' PSK'}}}
```
```Python

## get the essid for the Fifth Element!
essid = iw.list[5]['ESSID']
print(essid)
```
```Bash
Out: 'WontYouBeMyNeighbor'
```
```Python

addr = iw.list[5]['Address']
print(addr)
```
```Bash
Out: '3C:G9:79:12:2B:E6'
```
```Python

ie = iw.list[5]['IE']
print(ie)
```
```Bash
Out:
{'IEEE 802.11i/WPA2 Version 1': {'Authentication Suites (1) ': ' PSK',
                                  'Group Cipher ': ' CCMP',
                                  'Pairwise Ciphers (1) ': ' CCMP'},
  'WPA Version 1': {'Authentication Suites (1) ': ' PSK',
                    'Group Cipher ': ' CCMP',
                    'Pairwise Ciphers (1) ': ' CCMP'}}
```
```Python

## if you need a list of actual values from the list
iw.list_of('ESSID')
```
```Bash
Out:
('jimmys crab shack',
 'Two-Ton Tommys Towing',
 'Slim Pickens')
```
```Python

## so you can do things like.
if 'Slim Pickens' in iw.list_of('ESSID'): ## will be True

```

### Using two cards

```Python

from pywlist import iwlist

iw = iwlsit(interface=None)

print(iw.cards())
```
```Bash
Out: ['lo', 'wlp3s0', 'tun0', 'enp4s0', 'wlx250ce7']
```
```Python

iw.iface = 'wlx250ce7'

iw2 = iwlist(interface='wlp3s0')

```

Thats it for the iwlist class. 


### Testing:


I created pywlist_test.py to aid in the continuity between Linux's iwlist <interface> scanning output and the iwlist(class).list dictionary object. 

There is no good way to compare the output other than human verification so this program writes both a text file and a .json file. Filenames are created using date and time in their own separate directories.

The json file is a good enough representation of a python dictionary although without tuples but Arrays are there in their stead.

You can create the comparison files by bash:

```Bash

$ python3.7 pywlist_test.py
```


You can do it in a python3.7 shell or ipython equivalent.

```Python

from pywlist_test import pytest

## Setup and run the iwlist class with the added files save functions
iw = iwtest()

## you can now look in the same directory your pywlist_test.py file is in and you should see a 
## folder called tests. Inside you'll find folders labeled by date and time.  

## Inside will be your comparison files

## If you want to make a new set of json and iwlist <interface> scanning text files all you have
## to do is rescan.

iw.scan()
```

That's it. I promise!